import { Routes, Route, BrowserRouter } from 'react-router-dom'
import { lazy } from 'react';
import Test from './views/test';
const NavList = lazy(() => import('./components/layout/nav/NavList'))
const Article = lazy(() => import('./views/article/Article'))
const Card = lazy(() => import('./components/Card/Card'))
const Header = lazy(() => import('./components/head/Header'))
const Video = lazy(() => import('./views/video/Video'))
const VideoDetail = lazy(() => import('./views/video/VideoDetail'))
const Editor = lazy(() => import('./components/markdown/Editor'))
const Login = lazy(() => import('./views/login/Login'))
const Code = lazy(() => import('./views/code/Code'))
const Talk = lazy(() => import('./views/talk/Talk'))
const TalkDetail = lazy(() => import('./views/talk/TalkDetail'))
const Picwall = lazy(() => import('./views/picwall/Picwall'))
const Music = lazy(() => import('./views/music'))
const ArticleDetail = lazy(() => import('./views/article/ArticleDetail'))
const CodeDetail = lazy(() => import('./views/code/CodeDetail'))
const CodeProblem = lazy(() => import('./views/code/CodeProblem'))
const Handler = lazy(() => import('./views/handler/handler'))
export default function App() {
    return (
        <div className='dark:text-light-100   dark:bg-dark-900'>
            <Header />
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Article />} />
                    <Route path="/navlist" element={<NavList />} />
                    <Route path="/card" element={<Card />} />
                    <Route path="/article" element={<Article />}/>
                    <Route path='/article/:name' element={<ArticleDetail/>}/>
                    <Route path="/video" element={<Video/>}/>
                    <Route path='/videoDetail' element={<VideoDetail/>}/>
                    <Route path='/editor' element={<Editor/>}/>
                    <Route path='/login' element={<Login/>}/>
                    <Route path='/code' element={<Code/>}/>
                    <Route path='/talk' element={<Talk/>}/>
                    <Route path='/talk-detail' element={<TalkDetail/>}/>
                    <Route path='/pic-wall' element={<Picwall/>}/>
                    <Route path='/music' element={<Music/>}/>
                    <Route path='/codeproblem' element={<CodeProblem/>}/>
                    <Route path='/codeproblem/:id' element={<CodeDetail/>}/>
                    <Route path='/secret' element={<Handler/>}/>
                    <Route path='/test' element={<Test/>}/>
                </Routes>
            </BrowserRouter>
        </div>
    )
}