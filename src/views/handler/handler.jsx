import ReactECharts from 'echarts-for-react';
import { useEffect, useState, useRef } from 'react';
import Editor from "@monaco-editor/react";
import { getTalk } from '../../api';
import { saveTalk, saveCode } from '../../api';
export default function Handler() {
    const [value, setValue] = useState("")
    const [option, setOption] = useState({})

    const codeInfo = useRef(null)
    const codeTag = useRef(null)
    const [addCode, setAddCode] = useState(false)
    const [isCode, setIsCode] = useState(false)
    // useEffect(() => {
    //     const set = new Set()
    //     const map = new Map()
    //     getTalk()
    //         .then((res) => {
    //             const nameList = res.data.data
    //             nameList.map(item => {
    //                 console.log('item', item.name);
    //                 set.add(item.name)
    //                 if (!map.has(item.name)) {
    //                     map.set(item.name, 1)
    //                 } else {
    //                     let much = map.get(item.name)
    //                     console.log('much', much);
    //                     map.set(item.name, much + 1)
    //                 }
    //                 console.log('map', map);
    //             })
    //             setOption({
    //                 title: { text: '耳语' },
    //                 tooltip: {},
    //                 legend: {
    //                     data: ['数量']
    //                 },
    //                 xAxis: {
    //                     data: Array.from(set)
    //                 },
    //                 yAxis: {},
    //                 series: [{
    //                     name: '数量',
    //                     type: 'bar',
    //                     data: Array.from(map.values())
    //                 }]
    //             })
    //         })
    // }, [])

    const editor = () => {
        setAddCode(true)
    }
    const toCode = () => {
        setIsCode(true)
    }


    const handleEditorChange = (code) => {
        setValue(code)
        console.log(code);
    }

    const submit = () => {
        const info = codeInfo.current?.value || ''
        const tag = codeTag.current?.value || ''
        const code = value || ''
        saveCode(code,info,tag)
            .then((res) => {
                console.log('res',res);
            })
    }
 

    return (
        <div className='mt-8 flex flex-row'>
            <div className='w-1000px mx-auto flex flex-row'>
                <div className='w-80px h-600px bg-gray-100 flex flex-col'>
                    <span className='mx-auto mt-5 cursor-pointer'><svg t="1667276799900" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3978" width="60" height="60"><path d="M997.419886 608.021943L748.046629 422.5024l140.639085-191.151543a30.178743 30.178743 0 0 0 5.573486-22.908343 202.4448 202.4448 0 0 0-201.4208-169.545143 216.502857 216.502857 0 0 0-32.680229 2.530743 31.085714 31.085714 0 0 0-19.631542 11.454172l-143.872 182.652343L251.962514 53.467429a31.5392 31.5392 0 0 0-43.607771 6.0416L20.202057 306.029714a30.471314 30.471314 0 0 0 6.129372 43.037257l240.2304 178.688-92.672 117.701486a30.325029 30.325029 0 0 0-5.851429 12.551314L106.130286 947.887543a30.72 30.72 0 0 0 11.044571 30.383543 31.597714 31.597714 0 0 0 19.412114 6.729143 30.1056 30.1056 0 0 0 13.048686-2.925715l273.802972-125.176685a30.6176 30.6176 0 0 0 12.039314-9.801143l92.233143-125.293715L771.847314 903.460571a31.583086 31.583086 0 0 0 18.7392 6.173258 31.085714 31.085714 0 0 0 24.868572-12.229486L1003.52 650.9568a30.471314 30.471314 0 0 0-6.100114-42.934857zM681.691429 100.688457a142.058057 142.058057 0 0 1 148.670171 105.472l-59.977143 81.481143a200.967314 200.967314 0 0 0-62.683428-72.338286 205.355886 205.355886 0 0 0-88.107886-35.898514zM88.693029 318.464l150.674285-197.485714 40.711315 30.281143-31.451429 41.0624a30.485943 30.485943 0 0 0 6.129371 43.051885 31.553829 31.553829 0 0 0 18.7392 6.173257 31.056457 31.056457 0 0 0 24.868572-12.229485l31.451428-41.179429 46.065372 34.245486-29.257143 38.429257a30.72 30.72 0 0 0 24.868571 49.210514 31.085714 31.085714 0 0 0 24.868572-12.214857l29.257143-38.429257 33.016685 24.546743-153.790171 195.203657z m91.121371 582.085486l41.047771-192.234057c0.775314-0.1024 1.462857-0.1024 2.106515-0.219429a119.442286 119.442286 0 0 1 136.850285 97.660343 33.28 33.28 0 0 0 3.905829 10.678857z m235.329829-130.194286a182.0672 182.0672 0 0 0-162.494172-124.840229L572.401371 239.469714a142.7456 142.7456 0 0 1 98.816 25.760915 138.708114 138.708114 0 0 1 55.208229 82.139428l-44.9536 61.103543c-0.570514 0.599771-266.3424 361.823086-266.3424 361.823086z m369.269028 65.7408l-220.16-163.84L711.4752 472.210286l32.343771 23.990857-33.133714 43.388343a30.485943 30.485943 0 0 0 6.144 43.037257 31.553829 31.553829 0 0 0 18.7392 6.173257 31.071086 31.071086 0 0 0 24.868572-12.229486l33.119085-43.373714 43.885715 32.592457-31.217372 40.842972a30.485943 30.485943 0 0 0 6.129372 43.051885 31.5392 31.5392 0 0 0 18.7392 6.158629 31.085714 31.085714 0 0 0 24.868571-12.214857l31.232-40.842972 48.186514 35.781486z" p-id="3979"></path></svg></span>
                    <span className='mx-auto mt-5 cursor-pointer'><svg t="1667276595984" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2609" width="60" height="60"><path d="M213.333333 341.333333h85.333334v85.333334h-85.333334zM405.333333 341.333333h85.333334v85.333334h-85.333334zM597.333333 341.333333h85.333334v85.333334h-85.333334z" p-id="2610"></path><path d="M853.333333 768V42.666667H42.666667v725.333333h128l-42.666667 85.333333h85.333333l42.666667-85.333333h597.333333zM128 682.666667V128h640v554.666667H128z" p-id="2611"></path><path d="M896 213.333333v597.333334H384v85.333333h384l42.666667 85.333333h85.333333l-42.666667-85.333333h128V213.333333z" p-id="2612"></path></svg></span>
                    <span className='mx-auto mt-5 cursor-pointer' onClick={toCode}><svg t="1667374742510" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="6040" width="60" height="60"><path d="M353.834667 268.501333L110.336 512l243.498667 243.498667 60.330666-60.330667L230.997333 512l183.168-183.168z m316.330666 486.997334L913.664 512l-243.498667-243.498667-60.330666 60.330667L793.002667 512l-183.168 183.168z" fill="" p-id="6041"></path></svg></span>
                </div>
                <div className='flex-1 h-600px w-600px bg-yellow-50'>
                    {isCode &&
                        <div className='flex flex-col w-650px mx-auto mt-5'>
                            <div className='flex flex-row'>
                                <textarea placeholder='介绍下代码' className='w-600px h-80px show-code-info text-lg' ref={codeInfo}></textarea>
                                <div className="flex flex-col ml-2" >
                                    <svg t="1667374742510" onClick={editor} viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="6040" width="32" height="32"><path d="M353.834667 268.501333L110.336 512l243.498667 243.498667 60.330666-60.330667L230.997333 512l183.168-183.168z m316.330666 486.997334L913.664 512l-243.498667-243.498667-60.330666 60.330667L793.002667 512l-183.168 183.168z" fill="" p-id="6041"></path></svg>
                                    <svg t="1667382562699" onClick={submit} className='mt-2 ml-0.2' viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="7250" width="30" height="30"><path d="M322.142929 293.741129H701.345327c27.122439 0 49.127436-22.004998 49.127437-49.127436s-22.004998-49.127436-49.127437-49.127436H322.142929c-27.122439 0-49.127436 22.004998-49.127437 49.127436s22.004998 49.127436 49.127437 49.127436zM322.142929 480.527736H701.345327c27.122439 0 49.127436-22.004998 49.127437-49.127436s-22.004998-49.127436-49.127437-49.127436H322.142929c-27.122439 0-49.127436 22.004998-49.127437 49.127436s22.004998 49.127436 49.127437 49.127436z" p-id="7251"></path><path d="M876.873563 0H147.126437C86.228886 0 36.589705 49.63918 36.589705 110.536732v802.414792c0 60.897551 49.63918 110.536732 110.536732 110.536732h157.105447c28.657671 0 52.197901-23.028486 52.197901-52.197901 0-28.657671-23.028486-52.197901-52.197901-52.197901H147.126437c-3.582209 0-6.652674-3.070465-6.652674-6.652674V110.536732c0-3.582209 3.070465-6.652674 6.652674-6.652674h729.747126c3.582209 0 6.652674 3.070465 6.652674 6.652674v802.414792c0 3.582209-3.070465 6.652674-6.652674 6.652674h-162.222888c-28.657671 0-52.197901 23.028486-52.197901 52.197901 0 28.657671 23.028486 52.197901 52.197901 52.197901h162.222888c60.897551 0 110.536732-49.63918 110.536732-110.536732V110.536732c0-60.897551-49.63918-110.536732-110.536732-110.536732z" p-id="7252"></path><path d="M671.664168 710.812594l-124.865567-124.865568c-19.446277-19.446277-50.662669-19.446277-69.597202 0l-124.865567 124.865568c-19.446277 19.446277-19.446277 50.662669 0 69.597201 19.446277 19.446277 50.662669 19.446277 69.597201 0l40.427787-40.427786v231.308346c0 27.122439 22.004998 49.127436 49.127436 49.127436s49.127436-22.004998 49.127436-49.127436v-231.308346l40.427786 40.427786c9.723138 9.723138 22.516742 14.328836 34.798601 14.328836 12.793603 0 25.075462-4.605697 34.798601-14.328836 20.469765-19.446277 20.469765-50.662669 1.023488-69.597201z" p-id="7253"></path></svg>
                                </div>
                            </div>
                            {addCode && <div>
                                <Editor
                                    height="450px"
                                    width="600px"
                                    options={{
                                        fontSize: '14px',
                                    }}
                                    theme="vs-dark"
                                    language="javascript"
                                    value={value}
                                    onChange={handleEditorChange}
                                />
                                <input placeholder='输入标签' className='w-600px h-30px text-lg' ref={codeTag}/>
                            </div>
                            }
                        </div>
                    }
                </div>
            </div>
            {/* <div className='w-200'>
                {Object.keys(option) && <div>
                    <ReactECharts
                        option={option}
                        style={{ height: 200 }}
                        opts={{ renderer: 'svg' }}
                    />
                    <div className='mx-auto w-100'>
                        <input placeholder='name' className='border-1px name' />
                        <input placeholder='content' className='border-1px content' />
                        <button className='bg-blue-300' onClick={talk}>add</button>
                    </div>
                </div>}
            </div>
            <div className='w-600px text-3xl flex flex-col'>
                <p>有意思的题目</p>
                <div className='flex flex-row'>
                    <textarea placeholder='info' className='info border-2px border-dark-100 w-400px h-200px' />
                    <div className='flex flex-col'>
                        <button className='w-100px h-160px bg-red-300' onClick={code}>提交</button>
                        <input placeholder='tag' className='tag w-100px text-base h-40px border-2px' />
                    </div>
                </div>
            </div> */}
        </div>
    )
}