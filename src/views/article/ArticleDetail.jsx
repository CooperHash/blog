import Preview from "../../components/markdown/Preview";
import { getSingleStandard } from "../../api";
import store from "../../store/configStore";
import { useEffect, useState } from "react"
export default function ArticleDetail() {
    const [source, setSource] = useState('')
    useEffect(() => {
        getSingleStandard(store.getState().id)
            .then((res) => {
                console.log(res.data.data[0]);
                setSource(res.data.data[0].content)
            }).catch((err) => {
                console.log(err);
            })
    }, [source])

    return (
        <div>
            <Preview source={source}></Preview>
            <div></div>
        </div>
    )
}